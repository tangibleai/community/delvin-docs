# Connect from TextIt

TextIt allows you to create conversational experiences for many platforms, including WhatsApp, Line, Telegram, and more.  To connect to Delvin from TextIt, you will need to create a Webhook block within your flow.


## Set up a Flow
1. Click on the Flows icon.
2. Click **New Flow**.
3. Fill out the Name and trigger keywords.  Currently, Delvin works best with **English** and requires you to use **Messaging** flows.
4. Click **Create**.

## Add a Webhook
1. The Delvin webhook will usually be called after a **Wait for Response** block.  Drag from the bottom-middle circle to create a new block.

2. In the form that appears, select **Call a webhook**.

3. Now you need to set up the webhook.  First fill out the form with the following information:
   - Request Type: POST
   - URL: https://staging.delvin.to/api/project/chatbot_response/
   - Result name: Delvin Result

4. Now click **HTTP Headers** and enter two additional headers:
   - `Content-Type: application/json`
   - `Authorization: Bearer {your-delvin-token-here}`

5. Finally, click into **POST Body**.  Update the request body with the following:
```
@(json(object(
  "contact", object(
    "uuid", contact.uuid,
  ),
  "results", foreach_value(results, extract_object, "value", "category"),
  "input", if(
    input,
    object(
        "text", 
        input.text
    )
  )
)))
```

6. Click **Ok**.


## Respond to the results
Your system will need to handle the result of the webhook request to Delvin.  We recommend doing this with at least 3 blocks:
1. Handle successful API responses for a continuing conversation
2. Handle successful API response for a finished conversation
3. Handle unsuccessful API responses

Textit automatically creates *Success* and *Failure* options for you.  Drag a block from the red circle under *Success* to define what to do with the results.  

For the success field, you need to handle cases where the conversation should continue and when it should stop.
1. Drag down from the *Success* option to create a new block.

2. Select **Split by custom expression**

3. Fill out the following settings, and click **Ok**:
   - Expression: `@webhook.json.status`
   - Condition: **has the phrase** / **completed** / categorize as **completed**

4. From the *Other* option in the **Split by custom expression** block, you need to send the message, wait for a response, and send it back to Delvin.
   -  Drag out to create a message.  Reference Delvin's response using `@webhook.json.message` in the message body.
   - After saving that message, drag to create a block with the **Wait for the contact to respond**.  Click **Ok**.
   - Draw a connection from the wait block back to the Delvin Webhook block.

5. When a conversation is finished, Delvin will send the *completed* keyword.  From the *split by expression* block's **Completed** option, drag to create a new block.  Add `@webhook.json.message` and click **Ok**.  You will need to handle any additional logic for your flow in the way that's most appropriate for your use case.
