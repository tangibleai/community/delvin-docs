# Connect from Turn.io

Turn.io is a chatbot development platform that facilitates conversations via WhatsApp.  You build conversational exchanges through journeys.  There are two types of journeys:
1. No-code - A low-code visual builder
2. Code - An IDE that uses a specialized version of Elixir

You may integrate your Turn.io conversational flows with Delvin using either approach.  However, both approaches will require the use of code.

As a result, we suggest using a code-based approach.  The code for a Delvin call can be set up in a code-based journey.  This can be called from other journeys.


## Create a journey
First, you should create a journey that the call to Delvin will live in.

1. Click **New Journey**.
2. Click **From Scratch**.
3. Click **Code**.


## Add the code for a Delvin call
The call to Delvin happens in three parts:
1. You need to collect the user's chat response.
2. You need to make a call to Delvin.
3. You need to handle the results.


### 1. Get a user's message
You need to use the `ask` keyword to create a question users can respond to.  Assign this to a variable like `user_response`
```elixir
card UserQuestion, then: DelvinAPICall do
  user_response = ask("What do you think?")
end
```

### 2. Make the Delvin request
```elixir
card DelvinAPICall, then: DelvinResult do
  # Collect the variables you want
  contact_uuid = '@event.message."_vnd".v1.chat.contact_uuid'

  # Make the request
  response =
    post("https://staging.delvin.to/api/project/chatbot_response/",
      body: """
      {"user_message": "@user_response", "contact": "@contact_uuid", "chat": "bye"}
      """,
      headers: [
        ["content-type", "application/json"],
        ["Authorization", "Bearer {your-delvin-token-here}"]
      ]
    )
end
```

:::tip

It's strongly recommended that you use the `contact_uuid` instead of the user's `whatsapp_id` as the Delvin `contact` value.  This will keep your user's privacy safer.

:::


### 3. Handle the results
Your system will need to handle the result of the API request to Delvin.  We recommend doing this with at least 3 cards:
1. Handle successful API responses for a continuing conversation
2. Handle successful API response for a finished conversation
3. Handle unsuccessful API responses

Check out the Delvin response object for more information about what the API sends back.

```elixir
# Successful response for a conversation the is finished
card DelvinResult when response.status == '200' and response.body.status == 'complete' do
    run_stack("your-journey-uuid")
end

# Successful response for a conversation that should continue
card DelvinResult when response.status == '200', then: DelvinAPICall:  do
  ask("""
  @response.message
  """)
end

# A catchall case for general errors
card DelvinResult do
  "Oh no..."
end
```

You can even add more precise error handling for specific statuses.  A good place to put these is above your "success" cards.
```elixir
card DelvinResult when response.status == '500' do
  text("""
  Hmm... we're having some trouble right now.
  """)
end
```


:::warning

**NOTE:** Depending on what variables you send to the API, you may not be able to preview the journey in the Turn.io preview interface.  In that case, connect your flow to an automation and trigger the flow in WhatsApp to test.

:::
